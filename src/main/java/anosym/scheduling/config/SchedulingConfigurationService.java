package anosym.scheduling.config;

import javax.annotation.Nonnull;

import khameleon.core.annotations.ConfigParam;
import khameleon.core.annotations.Default;
import khameleon.core.annotations.Info;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Feb 19, 2016, 10:26:17 PM
 */
public interface SchedulingConfigurationService {

	@Default("false")
	@Info("Whether the schedule, identified by the id is enabled")
	boolean isScheduleEnabled(@ConfigParam(name = "scheduleId") @Nonnull final String scheduleId);

	@Default("false")
	@Info("Whether the schedule, identified by the id is cancelled. A cancell schedule can only be restarted through server restart")
	boolean isScheduleCancelled(@ConfigParam(name = "scheduleId") @Nonnull final String scheduleId);

}
