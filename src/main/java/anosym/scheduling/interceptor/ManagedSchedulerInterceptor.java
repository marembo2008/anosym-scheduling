package anosym.scheduling.interceptor;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.String.format;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nonnull;

import anosym.scheduling.config.SchedulingConfigurationService;
import jakarta.annotation.Priority;
import jakarta.ejb.Timer;
import jakarta.inject.Inject;
import jakarta.interceptor.AroundTimeout;
import jakarta.interceptor.Interceptor;
import jakarta.interceptor.InvocationContext;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Feb 19, 2016, 10:35:56 PM
 */
@Interceptor
@ManagedScheduler
@Priority(Interceptor.Priority.APPLICATION)
public class ManagedSchedulerInterceptor implements Serializable {

	private static final long serialVersionUID = 4842339742658757815L;

	@Inject
	private SchedulingConfigurationService timerServiceConfigurationService;

	@Inject
	private Logger logger;

	@AroundTimeout
	public Object interceptTimeout(@Nonnull final InvocationContext invocationContext) throws Exception {
		checkNotNull(invocationContext, "The invocationContext must not be null");

		final ManagedScheduler managedScheduler = managedScheduler(invocationContext);

		if (isCancelled(invocationContext)) {
			final Object timer = invocationContext.getTimer();
			if (timer instanceof Timer) {
				final Timer scheduledTime = (Timer) timer;
				logger.log(Level.FINE, "Cancelling Timer: {0}", scheduledTime.getInfo());

				scheduledTime.cancel();
			}

			return null;
		}

		if (!isEnabled(invocationContext)) {
			logger.log(Level.FINE, "Scheduled jobs have been disabled: {0}", managedScheduler);

			return null;
		}

		return invocationContext.proceed();
	}

	private boolean isCancelled(@Nonnull final InvocationContext invocationContext) {
		final ManagedScheduler managedScheduler = managedScheduler(invocationContext);
		final String scheduleId = managedScheduler.scheduleId();

		return timerServiceConfigurationService.isScheduleCancelled(scheduleId);
	}

	private boolean isEnabled(@Nonnull final InvocationContext invocationContext) {
		final ManagedScheduler managedScheduler = managedScheduler(invocationContext);
		final String scheduleId = managedScheduler.scheduleId();

		return timerServiceConfigurationService.isScheduleEnabled(scheduleId);
	}

	@Nonnull
	private ManagedScheduler managedScheduler(@Nonnull final InvocationContext invocationContext) {
		final Method invokedMethod = invocationContext.getMethod();
		if (invokedMethod != null && invokedMethod.isAnnotationPresent(ManagedScheduler.class)) {
			return invokedMethod.getAnnotation(ManagedScheduler.class);
		}

		final Object target = invocationContext.getTarget();
		if (target != null && target.getClass().isAnnotationPresent(ManagedScheduler.class)) {
			return target.getClass().getAnnotation(ManagedScheduler.class);
		}

		throw new IllegalStateException(format("Invalid InvocationContext: %s", invocationContext));
	}

}
