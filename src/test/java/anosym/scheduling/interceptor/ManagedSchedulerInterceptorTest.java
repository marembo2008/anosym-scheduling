package anosym.scheduling.interceptor;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import anosym.scheduling.config.SchedulingConfigurationService;
import jakarta.interceptor.InvocationContext;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Feb 19, 2016, 11:05:32 PM
 */
@ExtendWith(MockitoExtension.class)
public class ManagedSchedulerInterceptorTest {

	@Mock
	private SchedulingConfigurationService schedulingConfigurationService;

	@Mock
	private Logger logger;

	@Mock
	private InvocationContext invocationContext;

	@InjectMocks
	private ManagedSchedulerInterceptor timerSchedulingService;

	@Test
	public void verifySchedulingEnabled() throws Exception {
		final Method invockedMethod = InterceptedClass.class.getDeclaredMethod("interceptedMethod1");
		when(invocationContext.getMethod()).thenReturn(invockedMethod);
		when(schedulingConfigurationService.isScheduleEnabled("scheduleId")).thenReturn(true);

		timerSchedulingService.interceptTimeout(invocationContext);
		verify(invocationContext, times(1)).proceed();
		verify(logger, times(0)).log(any(Level.class), anyString(), any(ManagedScheduler.class));
	}

	@Test
	public void verifySchedulingDisabled() throws Exception {
		final Method invockedMethod = InterceptedClass.class.getDeclaredMethod("interceptedMethod1");
		when(invocationContext.getMethod()).thenReturn(invockedMethod);
		when(schedulingConfigurationService.isScheduleEnabled("scheduleId")).thenReturn(false);

		timerSchedulingService.interceptTimeout(invocationContext);
		verify(invocationContext, times(0)).proceed();
		verify(logger, times(1)).log(any(Level.class), anyString(), any(ManagedScheduler.class));
	}

	@Test
	public void verifySchedulingCancelled() throws Exception {
		final Method invokedMethod = InterceptedClass.class.getDeclaredMethod("interceptedMethod1");
		when(invocationContext.getMethod()).thenReturn(invokedMethod);
		when(schedulingConfigurationService.isScheduleCancelled("scheduleId")).thenReturn(true);

		timerSchedulingService.interceptTimeout(invocationContext);
		verify(invocationContext, times(0)).proceed();
		verify(logger, times(0)).log(any(Level.class), anyString(), anyString());
	}

	private static class InterceptedClass {

		@ManagedScheduler(scheduleId = "scheduleId")
		public void interceptedMethod1() {
		}

	}

}
